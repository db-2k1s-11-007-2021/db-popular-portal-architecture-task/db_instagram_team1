--serializable

begin transaction;

set transaction isolation level serializable


update Items set item_count = item_count - 1
where Name = 'Gym equipment'

commit transaction;

--пример когда человек купил 1 единицу товара, и его количество меняется соответственно
-- уровни ниже не подходят, т.к. возможна покупка 1 единицы товара одновременно 2 пользователями
