--уровень read commited

begin transaction;

set transaction isolation level read committed   -- часто этот уровень используется по умолчанию
select item_count, price
from Items
where ID = '1337'

commit transaction;

--нам необходимо увидеть сколько предметов осталось на складе
--(если товар уже оплачен, то транзакция с оплатой завершится и количество товара поменяется в нашем запросе, если он
--просто лежит в корзине у кого-то, не факт что человек купит)

-- (read commited уровень подходит для случая когда товар добавленный в корзину не закрепляется за пользователем и может
-- быть куплен кем угодно в этот момент)


--1 КОНСОЛЬ
BEGIN;
UPDATE ITEMS SET PRICE = PRICE * 0,9
SELECT * FROM ITEMS

--2 КОНСОЛЬ
BEGIN;
UPDATE ITEMS SET PRICE = PRICE * 0,8

--ПЕРЕХОДИМ В 1 КОНCОЛЬ И делаем коммит
COMMIT;

--переходим  НА 2 КОНСОЛЬ И ВЫВОДИМ
SELECT PRICE FROM ITEMS WHERE ITEM_COUNT < 5
--в итоге окажется что были произведены оба изменения из обоих коммитов
--завершим 2 транзакцию
end;
