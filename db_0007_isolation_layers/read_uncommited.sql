--уровень read uncommited

begin transaction;

set transaction isolation level read uncommitted

insert into Items(id, name, amount, price, warehouse_id)
VALUES ('123490', 'чайник с функциями', '9', '300', '1337');

commit transaction;

/*
read uncommited можно исользовать если объем данных большой и/или данные редко изменяются

 в этом случае мы добавим новое поле с товаром, а так как другие поля никак его не использовали ранее, то
 и ничего страшного
 в использовании уровня read uncommited нет ( уровень выше можно использовать но есть ли в этом смысл?)
*/


--пример когда все может сломаться
--откроем 2 консоли и выполним команды

--консоль 1
begin transaction ;
set transaction isolation level read uncommitted

update Items set price = price * 0,9

--при выполнении select'a все цены будут на 10 % меньше, как и ожидалось
-- то есть покажутся все изменения в пределах транзакции

--консоль 2
begin transaction;
set transaction isolation level read uncommitted

--но если выполнить следующую команду:
select price from Items

--то мы увидим что ничего не изменилось ( т.к транзакция в 1 консоли не закоммичена,
-- следовательно данные в самой таблице не изменились)
